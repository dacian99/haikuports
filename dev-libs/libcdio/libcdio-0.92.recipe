SUMMARY="A library to encapsulate CD-ROM reading and control"
DESCRIPTION="
The libcdio package contains a library for CD-ROM and CD image \
access. Applications wishing to be oblivious of the OS- and \
device-dependent properties of a CD-ROM or of the specific details of \
various CD-image formats may benefit from using this library.
"
LICENSE="GNU GPL v3"
COPYRIGHT="2002-2008,2010,2012, Rocky Bernstein and Herbert Valerio Riedel."
HOMEPAGE="http://www.gnu.org/software/libcdio"
SRC_URI="ftp://ftp.gnu.org/gnu/libcdio/libcdio-0.92.tar.gz"
CHECKSUM_SHA256="3336bf352aff43b5023e41a6d23525172a3b4ee433c467bd0fab5b8becb33fad"
REVISION="1"
ARCHITECTURES="x86_gcc2 x86 x86_64"
SECONDARY_ARCHITECTURES="x86_gcc2 x86"

PATCHES="libcdio-0.92.patchset"

PROVIDES="
	libcdio$secondaryArchSuffix = $portVersion
	cmd:cdda_player
	cmd:cd_drive
	cmd:cd_info
	cmd:cd_read
	cmd:iso_info
	cmd:iso_read
	cmd:mmc_tool
	lib:libcdio$secondaryArchSuffix = 15.0.0 compat >= 15
	lib:libcdio++$secondaryArchSuffix = 0.0.2 compat >= 0
	lib:libiso9660$secondaryArchSuffix = 9.0.0 compat >= 9
	lib:libiso9660++$secondaryArchSuffix = 0.0.0 compat >= 0
	lib:libudf$secondaryArchSuffix = 0.0.0 compat >= 0
	"

REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libiconv$secondaryArchSuffix
	lib:libncurses$secondaryArchSuffix
	"
BUILD_REQUIRES="
	devel:libiconv$secondaryArchSuffix
	devel:libncurses$secondaryArchSuffix
	"
BUILD_PREREQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	cmd:make
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:libtoolize
	cmd:autoconf
	cmd:automake
	"

BUILD()
{
	libtoolize --force --copy --install
	aclocal -I m4
	autoconf
	automake
	runConfigure ./configure
	make   
}

INSTALL()
{
	make install

	# set up the develop directory correctly
	prepareInstalledDevelLibs libcdio libcdio++ libudf libiso9660 libiso9660++
	fixPkgconfig

	# devel package
	packageEntries devel \
		$developDir
}

TEST()
{
	make check
}

# ----- devel package -------------------------------------------------------

PROVIDES_devel="
	libcdio${secondaryArchSuffix}_devel = $portVersion
	devel:libcdio${secondaryArchSuffix} = 15.0.0 compat >= 15
	devel:libcdio++${secondaryArchSuffix} = 0.0.2 compat >= 0
	devel:libiso9660${secondaryArchSuffix} = 9.0.0 compat >= 9
	devel:libiso9660++${secondaryArchSuffix} = 0.0.0 compat >= 0
	devel:libudf${secondaryArchSuffix} = 0.0.0 compat >= 0
	"
REQUIRES_devel="
	libcdio${secondaryArchSuffix} == $portVersion base
	"
